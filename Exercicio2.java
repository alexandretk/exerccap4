public class Exercicio2 {
//Chamar a classe de Funcionario

	private String nome;
	private String departamento;
	private double salario;
	private String dataEntrada;
	private String rg;



	public void recebeAumento (double aumento) {
		salario+= aumento;
	}

	public double calculaGanhoAnual () {
		return 12*salario;
	}


// Setters e Getters
	public void setNome(String nome){
		this.nome=nome;
	}

	public String getNome() {
		return nome;
	}

	public void setDepartamento(String departamento) {
		this.departamento=departamento;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setSalario(double salario){
		this.salario=salario;
	}

	public double getSalario() {
		return salario;
	}

	public void setDataEntrada(String dataEntrada){
		this.dataEntrada=dataEntrada;
	}

	public String getDataEntrada() {
		return dataEntrada;
	}

	public void setRG(String rg){
		this.rg=rg;
	}

	public String getRG() {
		return rg;
	}

}


