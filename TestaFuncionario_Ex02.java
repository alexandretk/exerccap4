public class TestaFuncionario_Ex02 {

	public static void main(String[] args) {
		Exercicio2 f1 = new Exercicio2();
		f1.setNome("Hugo");
		f1.setSalario(100);
		f1.recebeAumento(50);
		f1.setDepartamento("Engenharia");
		f1.setDataEntrada("11/09/2014");
		f1.setRG("111111");

		System.out.println("Salario atual: " + f1.getSalario() );
		System.out.println("Ganho anual: " + f1.calculaGanhoAnual() );
		System.out.println(" Departamento do funcionario: " + f1.getDepartamento());
		System.out.println(" Data de entrada no banco: " + f1.getDataEntrada());
		System.out.println(" RG do funcionario: " + f1.getRG());
	}

}
