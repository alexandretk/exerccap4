public class TestaFuncionario_Ex03 {

	public static void main(String[] args) {
		Exercicio3_Funcionario f1 = new Exercicio3_Funcionario();
		f1.setNome("Hugo");
		f1.setSalario(100);
		f1.recebeAumento(50);
		f1.setDepartamento("Engenharia");
		f1.setDataEntrada("11/09/2014");
		f1.setRG("111111");

		f1.mostra();
	}

}
