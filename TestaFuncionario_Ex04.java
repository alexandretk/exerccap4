/* 
	if (f1 == f2)
	{
		System.out.println("Iguais");
	} else
	{
		System.out.println("Diferentes");
	}

//Saida: Diferentes

*/

public class TestaFuncionario_Ex04 {

	public static void main(String[] args) {
		Exercicio4_Funcionario f1 = new Exercicio4_Funcionario();
		f1.setNome("Hugo");
		f1.setSalario(100);
		f1.recebeAumento(50);
		f1.setDepartamento("Engenharia");
		f1.setDataEntrada("11/09/2014");
		f1.setRG("111111");

		Exercicio4_Funcionario f2 = new Exercicio4_Funcionario();
		f2.setNome("Hugo");
		f2.setSalario(100);
		f2.recebeAumento(50);
		f2.setDepartamento("Engenharia");
		f2.setDataEntrada("11/09/2014");
		f2.setRG("111111");

	if (f1 == f2)
	{
		System.out.println("Iguais");
	} else
	{
		System.out.println("Diferentes");
	}
	
	}

}
