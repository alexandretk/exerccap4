/* 
	Funcionario f2 = f1;

	if (f1 == f2)
	{
		System.out.println("Iguais");
	} else
	{
		System.out.println("Diferentes");
	}

//Saida: IGUAIS

*/
public class TestaFuncionario_Ex05 {

	public static void main(String[] args) {
		Exercicio5_Funcionario f1 = new Exercicio5_Funcionario();
		f1.setNome("Hugo");
		f1.setSalario(100);
		f1.recebeAumento(50);
		f1.setDepartamento("Engenharia");
		f1.setDataEntrada("11/09/2014");
		f1.setRG("111111");


		Exercicio5_Funcionario f2 = f1;
		

		

	if (f1 == f2)
	{
		System.out.println("Iguais");
	} else
	{
		System.out.println("Diferentes");
	}
	
	}

}
